const express = require('express');
const router = express.Router();
const apiAdapter = require('../../config/api-adapter/api-adapter');
const User = require('../../models/users.model');
const validate = require('../middlewares/validate.middleware');
const jwt = require('../../utils/jwt.util');
const bcrypt = require('../../utils/bcrypt.util');
const config = require('config');

const s3Service = require('../../services/s3')

router.post('/api/album', async (req, res, next) => {
    try {
        const albumTitle = req.body.title;
        const albumPics = req.body.pictures;
        const picUrls = [];

        for (let pic of albumPics) {
            const { name, type } = pic;
            picUrls.push(await s3Service.getSignedUrl(name, type));
        }

        console.log('.... generated presigned urls ....', picUrls)

        /*
            // create album with file names
            // should be stored in mongodb
            testAlbum = {
                files: [{}], // name, pre-signed-url: 
                describtion: 
                createdAt:
            }        
        */
        return res.status(200).json({
            sucess: true,
            msg: "Album is created successfully",
            preSignedUrls: picUrls
        })


    } catch (err) {
        next(err);
    }
})

module.exports = router;

