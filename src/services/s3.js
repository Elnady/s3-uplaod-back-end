const AWS = require('aws-sdk');
const config = require('config');
// refactor, move to seperate file 
const s3 = new AWS.S3({
    accessKeyId: config.get('AWS_ACCESS_KEY_ID'),
    secretAccessKey: config.get('AWS_SECRET_KEY')
    // useAccelerateEndpoint: true
});
///////////////////////////////



module.exports = {
    getSignedUrl(fileName, fileType) {
    
        return new Promise((resolve, reject) => {
            s3.getSignedUrl('putObject', {
                Bucket: config.get('signedURLsParams.Bucket'),
                Expires: config.get('signedURLsParams.Expires'),
                Key: fileName,
                ContentType: fileType
                // ACL: config.get('signedURLsParams.ACL'),
                // ContentType: config.get('signedURLsParams.ContentType')

            }, (err, url) => {
                if (err) {
                    console.log("$$$$ err $$$$", err);
                    reject('Something went wrong !');
                }

                resolve({ url, fileName });
            })
        })
    }


}