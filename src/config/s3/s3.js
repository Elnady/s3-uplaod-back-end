const AWS = require('aws-sdk');
const config = require('config');

// factory function
module.exports = () => {
    return new AWS.S3({
        accessKeyId: config.get('AWS_ACCESS_KEY_ID'),
        secretAccessKey: config.get('AWS_SECRET_KEY'),
    }) 
}
